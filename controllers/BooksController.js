var Book        = require('../models/Book.js');
var Author      = require('../models/Author.js');

class BooksController {
  
  index(req, res, next) {
    Book.find({}, 'title', (err, books) => {
      if(err) return next(err);
			var myBooks = [];
			for(var i = 0;i<books.length;i++) {
				myBooks.push({
					"id": books[i]._id,
					"title": books[i].title,
				});
			}
      res.json(myBooks);
    });
  }
  
  show(req, res, next) {
    Book.findById(req.params.id).populate('author').exec((err, book) => {
      if (err) return next(err);
      if (!book) return res.status(404).json({
        success: false,
        message: 'Book not found'
      });
      res.json(book);
    });
  }

  create(req, res, next) {
    new Book({
      title: req.body.title,
      author: req.body.author
    }).save((err, book) => {
      if (err) return next(err);
      res.json({
        message: 'Book added!',
        book: book,
      });
    });
  }

  update(req, res, next) {
    var id = req.params.id;
    var title = req.body.title;
    var newAuthor = req.body.author;
    Book.findById(id, (err, book) => {
      var oldAuthor = book.author;
      book.update({ $set: {
        title: title,
        author: newAuthor
      }}, (err) => {
        if (err) return next(err);
        if (oldAuthor != newAuthor) {
          console.log("Authors changed");
          Book.changeAuthors(book._id, oldAuthor, newAuthor);
        };
        res.json({
          message: "Book changed!",
          book: book,
        });
      });
    });
  }

  delete(req, res, next) {
    Book.findOneAndRemove({'_id': req.params.id}, (err, book) => {
      if (err) return next(err);

      if (!book) return res.json({
        success: false,
        message: 'Book not found.'
      });

      console.log("Deleting book: "+ book);

      // remove book from its authors books
      Author.findByIdAndUpdate(book.author, {$pull: {books: book.id}}, (err, author) => {
        if (err) return console.log(err);
        console.log("Author after update: " + author);
        res.json({
          success: true,
          message: 'Book deleted.'
        });
      });
    });
  }

  search(req, res, next) {
    var title = req.params.title
    var regExp = new RegExp();

    var isValid = true;
    try {
        regExp = new RegExp(title, "i")
    } catch(e) {
        isValid = false;
    }

    if(!isValid) return res.json("Invalid search term.");
    Book.find({title: new RegExp(title, "i")}, (err, books) => {
      res.json(books);
    });
  }
}

module.exports = new BooksController();
