var express         = require('express'),
    exphbs          = require('express-handlebars'),
    methodOverride  = require('method-override'),
    morgan          = require('morgan'),
    formidable      = require('formidable'),
    mongoose        = require('mongoose'),
    jwt             = require('jsonwebtoken'),
    expressJWT      = require('express-jwt'),
    validator       = require('express-validator'),
    expressSession  = require('express-session'),
    util            = require('util'),
		cors						= require('cors');

var credentials     = require('./credentials.js'),
    User            = require('./models/User.js'),
    Book            = require('./models/Book.js'),
    Author          = require('./models/Author.js'),
    auth            = require('./lib/auth.js');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var cookieParser = require('cookie-parser');

class Bootstrap {

  init() {
    this.app = express();
    this.app.set('port', process.env.PORT || 8081);
    this.app.set('jwtSecret', credentials.jwt.secret);
    this.app.disable('x-powered-by');
  }

  misc() {
    this.app.use(morgan('dev'));
    this.app.use(require('body-parser').urlencoded({ extended: true }));
    this.app.use(require('body-parser').json());
    this.app.use(validator());

    this.app.use(methodOverride('_method'));

    this.app.use(cookieParser());
    this.app.use(expressSession({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: false
    }));
    this.app.use(passport.initialize());
    this.app.use(passport.session());

    passport.use(new LocalStrategy(User.authenticate()));
    passport.serializeUser(User.serializeUser());
    passport.deserializeUser(User.deserializeUser());
  }

	views() {
		this.app.set('views', __dirname + '/views');
		this.app.set('view engine', 'jsx');
		this.app.engine('jsx', require('express-react-views').createEngine());
	}
	
  db() {
    var options = {
      user: credentials.mongo.dev.user,
      pass: credentials.mongo.dev.pass
    }
    mongoose.connect(credentials.mongo.dev.url, options);
    mongoose.Promise = require('bluebird');
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
      console.log("Connected to mongodb");
    });
  }

  routes() {
		this.app.use(cors());
		
    this.app.use('/books',    require('./routes/books.js'));
    this.app.use('/authors',  require('./routes/authors.js'));
    this.app.use('/users',    require('./routes/users.js'));
    this.app.use('/', require('./routes/index.js'));
		
    this.app.use((req, res, next) => {
    	res.status(404);
    	res.json({
        message: '404 - Not found!'
      });
    });

    this.app.use((err, req, res, next) => {
    	console.error(err.stack);
    	res.status(500);
    	res.json({
        message: '500 - Internal server error!'
      });
    });
  }

  run() {
    this.init();
    this.misc();
		this.views();
    this.db();
    this.routes();
    return this.app;
  }
}

module.exports = new Bootstrap();
